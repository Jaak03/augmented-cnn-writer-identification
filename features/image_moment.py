from variables.features import image_moment as v

def image_moment( image ):
    height, width = image.shape

    moment_sums = {
        "00": 0,
        "01": 0,
        "10": 0,
        "11": 0,
    }

    # Up-Down
    for y in range(height):
        # Left-Right
        for x in range(width):
            value = image[y][x]
            if (value < v.THRESHOLD):
                moment_sums["00"] += value
                moment_sums["01"] += y * value
                moment_sums["10"] += x * value
                moment_sums["11"] += y * x * value

    # Normalize the coordinates in relation to the center of the image.
    def normalize(axis, length):
        return moment_sums[axis] / moment_sums["00"] / (length / 2)

    return [
        normalize("01", height),
        normalize("10", width),
    ]
